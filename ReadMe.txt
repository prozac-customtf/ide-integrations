//=======================================================================================//
// ReadMe.txt for the IDE-Integrations GitLab Prozac-CustomTF project                    //
//---------------------------------------------------------------------------------------//
// Original URL-> https://gitlab.com/prozac-customtf/ide-integrations                    //
//=======================================================================================//

This gitlab project includes useful files to develop our source code in Linux (Visual
Studio Code) and in Windows (EditPlus 2.11 for QuakeC-IDE-like batch mod user tools)

NOTES FOR LINUX/VISUAL STUDIO CODE:

If you gonna use vscode to edit quakec or quake in general, set default file encoding to 
"ANSI 1252" and enable auto-guess file encoding in vscode settings, otherwise, quake higher 
chars will get messed when you edit the files. In the contents of this repository you 
will find the tasks.json for the different projects, they should be placed inside a ".vscode"
directory on the source location. They are quite system dependant as they use my personal 
places/dirs for copying the binaries results, but I've included these tasks.json's because 
they are useful as starting point in vscode, after editing some of them for your environment 
or dir structure. For this reason, mostly the default build task, will fail on the last 
copying binary step if the tasks.json is tried to run unedited...

For example the PQWSV_tasks.json file should be renamed to tasks.json, and placed inside
a directory called ".vscode" inside the "src" directory of the Prozac's Quakeworld server
source code. From then and after making changes to adapt to your system and dir structure 
on the tasks, VS Code should list the tasks under the terminal menu. Maybe vs code needs a 
workspace too, place it in the same folder, "src".

The different tasks files are for compiling these corresponding projects:

ProzacMOD_tasks.json (VS Code Linux tasks for the Prozac CustomTF QuakeC Mod)
PQWSV_tasks.json (VS Code Linux tasks for the QuakeWorld server, MVDSV-based)
ProzacQW_tasks.json (VS Code Linux tasks for the Prozac special client)
FTEQCC_tasks.json (VS Code Linux tasks for the FTE QuakeC compiler)
CPPreQCC_tasks.json (VS Code Linux tasks for the Prozac QuakeC preprocessor)
CPQCCX_tasks.json (VS Code Linux tasks for the Prozac QuakeC Compiler)

NOTES FOR WINDOWS/EDITPLUS QUAKEC INTEGRATION:

The file "qc.stx" is an EditPlus (tested in 2.11) modified QuakeC syntax color hilight 
specifically for prozac QuakeC.

The regular expression for catching errors output from the configured user tools in 
editplus, like running "make.bat", is:

(.+.qc)p?:([0-9]+):

Use that, in the output pattern when configuring EditPlus user tools, also use 
"Tagged Expression 1" as the filename and "Tagged Expression 2" as line. Don't use column
or tagged expression 3. As our QuakeC binary tools don't provide that. That regular 
expression output pattern will catch all preprocessed or compiled *.QC and *.QCP and only
open the QC when a QCP is clicked too, that was intended for convenience, as when using
the original tools to compile QuakeC there is no need to edit QCP preprocesor intermediate
files at all, and you usually just need to open the corresponding QC file instead.

The function analizer feature in editPlus can be used with the following regular
expression:

.+\(.*\) ?(.+) [^!=<>]?=

That one isn't perfect, and lists as functions some code lines too. But it mostly works.
 
//=======================================================================================//
// ReadMe.txt originally written on 19/04/2023 by Sergi Fumanya Grunwaldt aka OfteN [cp] //
//=======================================================================================//
